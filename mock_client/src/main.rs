use std::{process, time::Duration};

use async_std::{fs, prelude::StreamExt, task::block_on};
use paho_mqtt as mqtt;

const MQTT_ADDRESS: &str = "tcp://192.168.20.99:1883";

const TOPICS: &[&str] = &[api::CONNECT, api::DISCONNECT, api::DOORBELL_ACTIVATED];
const QOS: &[i32] = &[1; TOPICS.len()];

mod api {
    pub const CONNECT: &str = "connect";
    pub const DISCONNECT: &str = "disconnect";
    pub const DOORBELL_ACTIVATED: &str = "doorbell_activated";
}

async fn run() {
    let create_opts = mqtt::CreateOptionsBuilder::new()
        .server_uri(MQTT_ADDRESS)
        .client_id("mock_client")
        .finalize();

    let mut mqtt_client = mqtt::AsyncClient::new(create_opts).unwrap_or_else(|e| {
        println!("Error creating the client: {:?}", e);
        process::exit(1);
    });

    let mut stream = mqtt_client.get_stream(25);
    let lwt = mqtt::Message::new("disconnect", "Async subscriber lost connection", mqtt::QOS_1);

    let conn_opts = mqtt::ConnectOptionsBuilder::with_mqtt_version(mqtt::MQTT_VERSION_5)
        .clean_start(true)
        .will_message(lwt)
        .automatic_reconnect(Duration::from_secs(1), Duration::from_secs(120))
        .finalize();

    println!("Connecting to the MQTT server...");
    mqtt_client.connect(conn_opts).await.unwrap();

    println!("Subscribing to topics...");

    let sub_opts = vec![mqtt::SubscribeOptions::with_retain_as_published(); TOPICS.len()];
    mqtt_client
        .subscribe_many_with_options(TOPICS, QOS, &sub_opts, None)
        .await
        .unwrap();

    println!("Ready for messages...");

    loop {
        if let Some(msg) = stream.next().await {
            if let Some(msg) = msg {
                println!("Received a message: {}", msg.topic());
                if msg.topic() == "doorbell_activated" {
                    let data = msg.payload();
                    fs::write("image.data", data).await;
                }
            }
        }
    }
}

fn main() {
    block_on(run());
}
