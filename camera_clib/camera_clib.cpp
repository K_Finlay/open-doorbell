//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- INCLUDES --------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#include <libcamera/libcamera.h>
#include <iostream>
#include <memory>
#include <sys/mman.h>

#include "camera_clib.h"
#include "event_loop.h"

//--------------------------------------------------------------------------------------------------
//-- PRIVATE CLASSES -------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

class CameraState {
public:
    static CameraState& instance() {
        static CameraState instance;
        return instance;
    }

    CameraState() = default;
    CameraState(CameraState const&)       = delete;
    void operator=(CameraState const&)  = delete;

    std::shared_ptr<libcamera::Camera> camera;
    EventLoop loop;

    float prevTime = 0.0f;
    ImageBuffer buffer;

    void (*callback)(ImageBuffer);

    void set_image_ptr(int descriptor, unsigned int id, unsigned int length, unsigned int offset) {
        if (buffer.imagePtr != nullptr) {
            munmap(buffer.imagePtr, buffer.length);
        }

        buffer.id = id;
        buffer.length = length;
        buffer.offset = offset;
        buffer.imagePtr = mmap(nullptr, length, PROT_READ, MAP_SHARED, descriptor, offset);;
    }
};

//--------------------------------------------------------------------------------------------------
//-- PRIVATE FUNCTIONS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

static void process_request(libcamera::Request* request);

//--------------------------------------------------------------------------------------------------

static void request_complete(libcamera::Request* request) {
    if (request->status() == libcamera::Request::RequestCancelled) {
        return;
    }

    CameraState::instance().loop.call_later([request] { return process_request(request); });
}

//--------------------------------------------------------------------------------------------------

static void process_request(libcamera::Request* request) {
    using namespace libcamera;

    auto& state = CameraState::instance();
    std::cout << "Request completed: " << request->toString() << std::endl;

    if (request->status() == Request::RequestCancelled) return;
    std::cout << "on req complete" << std::endl;
    for (const auto& stream_and_buf: request->buffers())
    {
        auto* stream = stream_and_buf.first;
        auto* buf = stream_and_buf.second;

        auto length = buf->planes()[0].length;
        auto offset = buf->planes()[0].offset;

        state.set_image_ptr(buf->planes()[0].fd.get(), buf->metadata().sequence, length, offset);
        state.callback(state.buffer);

        if (state.prevTime > 0)
        {
            auto delta_time = buf->metadata().timestamp - state.prevTime;
            double frameRate = 1e9 / delta_time;  // Calculate frame rate in frames per second
            std::cout << "  stream " << std::hex << size_t(stream) << " buffer " << std::hex << size_t(buf)
                      << " capture at " << std::dec << double(delta_time) / 1e6 << " ms" << std::endl;
            std::cout << "  FPS: " << frameRate << std::endl;
        }

        state.prevTime = buf->metadata().timestamp;
    }

    request->reuse(Request::ReuseBuffers);
    state.camera->queueRequest(request);
}

//--------------------------------------------------------------------------------------------------

std::string camera_name(libcamera::Camera* camera) {
    const libcamera::ControlList &props = camera->properties();
    std::string name;

    const auto &location = props.get(libcamera::properties::Location);
    if (location) {
        switch (*location) {
            case libcamera::properties::CameraLocationFront:
                name = "Internal front camera";
                break;
            case libcamera::properties::CameraLocationBack:
                name = "Internal back camera";
                break;
            case libcamera::properties::CameraLocationExternal:
                name = "External camera";
                const auto &model = props.get(libcamera::properties::Model);
                if (model) {
                    name = " '" + *model + "'";
                }
                break;
        }
    }

    name += " (" + camera->id() + ")";
    return name;
}

//--------------------------------------------------------------------------------------------------
//-- PUBLIC FUNCTIONS ------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

void start_streaming(CameraConfig camera_config, void (*callback)(ImageBuffer)) {
    using namespace libcamera;

    auto& state = CameraState::instance();

    // Create a new camera manager.
    auto camera_manager = std::make_unique<CameraManager>();
    camera_manager->start();

    // Find cameras connected to the system
    const auto& cameras = camera_manager->cameras();
    if (cameras.empty()) {
        std::cout << "No cameras were found" << std::endl;
        camera_manager->stop();
        return;
    }

    // Acquire the first camera
    auto& camera = state.camera;
    camera = cameras[0];
    camera->acquire();

    // Set the camera configuration
    auto config = camera->generateConfiguration({ camera_config.role });
    auto& stream_config = config->at(0);

    stream_config.size.width = camera_config.width;
    stream_config.size.height = camera_config.height;
    stream_config.pixelFormat = formats::RGB565;

    config->validate();

    // Debug code for listing available pixel formats
    for (auto& formats : stream_config.formats().pixelformats()) {
        std::cout << formats.toString() << std::endl;
    }

    camera->configure(config.get());

    // Allocate the frame buffers
    auto* allocator = new FrameBufferAllocator(camera);
    for (auto& cfg : *config) {
        int ret = allocator->allocate(cfg.stream());

        if (ret < 0) {
            std::cerr << "Can't allocate buffers" << std::endl;
            return;
        }

        auto allocated = allocator->buffers(cfg.stream()).size();
        std::cout << "Allocated " << allocated << " buffers for stream" << std::endl;
    }

    // Get the camera stream, and queue requests into the existing buffers.
    auto* stream = stream_config.stream();
    const auto& buffers = allocator->buffers(stream);
    std::vector<std::unique_ptr<Request>> requests;

    for (const auto & buffer : buffers) {
        auto request = camera->createRequest();

        if (!request) {
            std::cerr << "Can't create request" << std::endl;
            return;
        }

        int ret = request->addBuffer(stream, buffer.get());
        if (ret > 0) {
            std::cerr << "Can't set buffer for request" << std::endl;
            return;
        }

        auto& controls = request->controls();
        controls.set(controls::AeExposureMode, controls::AeExposureModeEnum::ExposureShort);

        requests.push_back(std::move(request));
    }

    // Connect callbacks
    camera->requestCompleted.connect(request_complete);

    camera->start();
    for (auto& request: requests) {
        camera->queueRequest(request.get());
    }

    // Start the main loop
    state.callback = callback;
    state.loop.exec();

    std::cout << "Shutting down" << std::endl;

    // Free all resources
    camera->stop();
    allocator->free(stream);
    delete allocator;
    camera->release();
    camera.reset();
    camera_manager->stop();
}

//--------------------------------------------------------------------------------------------------

void stop_streaming() {
    CameraState::instance().loop.exit();
}
