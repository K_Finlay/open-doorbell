//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

#pragma once

#include <libcamera/stream.h>

//--------------------------------------------------------------------------------------------------
//-- FORWARD DECLARATIONS --------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

namespace libcamera {
    class Camera;
}

//--------------------------------------------------------------------------------------------------
//-- ENUMS -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

struct CameraConfig {
    unsigned int width = 1280;
    unsigned int height = 720;
    libcamera::StreamRole role;
};

//enum class RCamConfig {
//    None,
//    StillCapture,
//    ViewFinder,
//    VideoRecording
//};

//--------------------------------------------------------------------------------------------------
//-- STRUCTS ---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

struct ImageBuffer {
    unsigned int id = 0;
    unsigned int length = 0;
    unsigned int offset = 0;
    void* imagePtr = nullptr;
};

//--------------------------------------------------------------------------------------------------
//-- FUNCTIONS -------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

void start_streaming(CameraConfig camera_config, void (*callback)(ImageBuffer));
void stop_streaming();
