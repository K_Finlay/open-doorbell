//
// Created by kyle on 20/05/23.
//

#include "event_loop.h"

#include <cassert>
#include <event2/event.h>
#include <event2/thread.h>

EventLoop* EventLoop::instance_ = nullptr;

EventLoop::EventLoop() {
    assert(!instance_);

    evthread_use_pthreads();
    event_ = event_base_new();
    instance_ = this;
}

EventLoop::~EventLoop() {
    instance_ = nullptr;

    event_base_free(event_);
    libevent_global_shutdown();
}

int EventLoop::exec() {
    exit_code_ = -1;
    exit_.store(false, std::memory_order::memory_order_seq_cst);

    while (!exit_.load(std::memory_order_seq_cst)) {
        dispatch_calls();
        event_base_loop(event_, EVLOOP_NO_EXIT_ON_EMPTY);
    }

    return exit_code_;
}

void EventLoop::exit(int code) {
    exit_code_ = code;
    exit_.store(true, std::memory_order_seq_cst);
    interrupt();
}

void EventLoop::interrupt() {
    event_base_loopbreak(event_);
}

void EventLoop::timeout_triggered(int fd, short event, void *arg) {
    auto* self = static_cast<EventLoop*>(arg);
    self->exit();
}

void EventLoop::timeout(unsigned int sec) {
    struct event *ev;
    struct timeval tv;

    tv.tv_sec = sec;
    tv.tv_usec = 0;
    ev = evtimer_new(event_, &timeout_triggered, this);
}

void EventLoop::call_later(const std::function<void()> &func) {
    {
        std::unique_lock<std::mutex> locker(lock_);
        calls_.push_back(func);
    }

    interrupt();
}

void EventLoop::dispatch_calls() {
    std::unique_lock<std::mutex> locker(lock_);

    for (auto iter = calls_.begin(); iter != calls_.end(); ) {

        // Check if the exit flag has been set, and return if so.
        // This prevents us from getting stuck in this loop if the machine can't keep up with the calls.
        if (exit_.load(std::memory_order::memory_order_seq_cst)) {
            return;
        }

        std::function<void()> call = std::move(*iter);
        iter = calls_.erase(iter);
        locker.unlock();
        call();
        locker.lock();
    }
}
