//
// Created by kyle on 20/05/23.
//

#ifndef RCAM_C_EVENT_LOOP_H
#define RCAM_C_EVENT_LOOP_H

#include <functional>
#include <atomic>
#include <list>
#include <mutex>

struct event_base;

class EventLoop {
public:
    EventLoop();
    ~EventLoop();

    void exit(int code = 0);
    int exec();

    void timeout(unsigned int sec);
    void call_later(const std::function<void()> &func);

private:

    static EventLoop* instance_;
    static void timeout_triggered(int fd, short event, void* arg);

    struct event_base* event_;
    std::atomic<bool> exit_;
    int exit_code_;

    std::list<std::function<void()>> calls_;
    std::mutex lock_;

    void interrupt();
    void dispatch_calls();

};

#endif //RCAM_C_EVENT_LOOP_H
