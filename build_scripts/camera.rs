//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

extern crate bindgen;

use std::{env, path::PathBuf};

pub fn build() {
    let dst = cmake::build("camera_clib");
    let header = "camera_clib/camera_clib.h";
    let link = [
        "camera_clib",
        "event",
        "event_pthreads",
        "camera",
        "camera-base",
        "stdc++",
    ];

    println!("cargo:rustc-link-search=native={}", dst.display());
    println!("cargo:rerun-if-changed=camera_clib/*");

    link.iter().for_each(|l| {
        println!("cargo:rustc-link-lib={}", l);
    });

    let bindings = bindgen::Builder::default()
        .header(header)
        .clang_arg("-I/usr/include/libcamera")
        .clang_arg("-xc++")
        .clang_arg("-std=c++17")
        .allowlist_file(header)
        .rustified_enum("libcamera::StreamRole")
        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        .generate()
        .expect("Unable to generate bindings");

    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("camera_clib_binding.rs"))
        .expect("Failed to write bindings");
}
