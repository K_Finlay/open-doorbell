use crate::hardware::camera::{camera_clib::libcamera_StreamRole, CameraConfig};

pub struct Config {
    pub ring_timeout_secs:     u64,
    pub touch_sensor_gpio_pin: u8,

    pub network_config: NetworkConfig,
    pub camera_config:  CameraConfig,
}

impl Default for Config {
    fn default() -> Self {
        Self {
            ring_timeout_secs:     30,
            touch_sensor_gpio_pin: 15,

            network_config: NetworkConfig {
                mqtt_client_id:      String::from("doorbell"),
                mqtt_broker_address: String::from("localhost"),
                mqtt_broker_port:    1883,
            },

            camera_config: CameraConfig {
                width:  1280,
                height: 720,
                role:   libcamera_StreamRole::VideoRecording,
            },
        }
    }
}

pub struct NetworkConfig {
    pub mqtt_client_id:      String,
    pub mqtt_broker_address: String,
    pub mqtt_broker_port:    u16,
}
