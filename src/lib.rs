//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

use std::{process, sync::Arc, time::Duration};

use async_std::task::block_on;
use log::error;
use parking_lot::{Mutex, RwLock};

use crate::hardware::{camera, touch_sensor::TouchSensor};

pub mod config;
pub mod hardware;
pub mod messenger;

use crate::{
    config::Config,
    hardware::touch_sensor::TouchState,
    messenger::{api, Messenger},
};

//--------------------------------------------------------------------------------------------------
//-- ENUMS -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[derive(PartialOrd, PartialEq)]
pub enum ErrorState {
    CameraMissing,
    NetworkUnavailable,
    ClientMissing,
}

//--------------------------------------------------------------------------------------------------

#[derive(PartialOrd, PartialEq)]
pub enum AppState {
    FirstRun,
    Startup,
    Idle,
    Ringing,
    Error(ErrorState),
    Shutdown,
}

//--------------------------------------------------------------------------------------------------
//-- FUNCTIONS -------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

pub async fn run() {
    env_logger::init();

    let state = Arc::new(RwLock::new(AppState::Startup));
    let config = Config::default();

    let messenger = Arc::new(Mutex::new(Messenger::new(&config.network_config).unwrap_or_else(|e| {
        error!("{}", e);
        // TODO: Set the LED's into an error state
        process::exit(1);
    })));

    // Start camera streaming
    let camera_thread = camera::start_streaming(config.camera_config);

    // Init the touch sensor
    let mut sensor = {
        let state = state.clone();
        let messenger = messenger.clone();
        TouchSensor::new(config.touch_sensor_gpio_pin, move |touch_state| {
            if touch_state == TouchState::Pressed {
                async_std::task::spawn(on_ring(config.ring_timeout_secs, state.clone(), messenger.clone()));
            }
        })
    };

    // Connect to MQTT broker
    if let Err(e) = messenger.lock().connect().await {
        error!("{}", e);
        // TODO: Set the LED's into an error state
        process::exit(1);
    }

    *state.write() = AppState::Idle;

    let messenger_task = Messenger::run(messenger.clone());

    while *state.read() != AppState::Shutdown {
        std::thread::sleep(Duration::from_millis(10));
        sensor.update();
    }

    let _res = messenger_task.await.cancel();
    let _res = messenger.lock().disconnect().await;
    camera::stop_streaming();
    camera_thread.join().unwrap();
}

fn main() {
    block_on(run());
}

//--------------------------------------------------------------------------------------------------

pub async fn on_ring(ring_timeout_secs: u64, app_state: Arc<RwLock<AppState>>, messenger: Arc<Mutex<Messenger>>) {
    // Early return if we are already ringing
    if *app_state.read() == AppState::Ringing {
        return;
    }

    *app_state.write() = AppState::Ringing;
    println!("Ring a ding a ling!");

    if let Err(e) = messenger
        .lock()
        .send_message(api::DOORBELL_ACTIVATED, camera::get_frame().data)
        .await
    {
        error!("{}", e);
    }

    async_std::task::sleep(Duration::from_secs(ring_timeout_secs)).await;
    *app_state.write() = AppState::Idle;
}
