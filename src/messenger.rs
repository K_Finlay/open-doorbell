use std::sync::Arc;

use async_std::prelude::StreamExt;
use paho_mqtt as mqtt;
use parking_lot::Mutex;
use thiserror::Error;

use crate::config::NetworkConfig;

pub mod api {
    /// Called on client connect.
    pub const CONNECT: &str = "connect";

    /// Called on client disconnect.
    pub const DISCONNECT: &str = "disconnect";

    /// Called when the doorbell is activated.
    pub const DOORBELL_ACTIVATED: &str = "doorbell_activated";
}

const TOPICS: &[&str] = &[api::CONNECT, api::DISCONNECT];
const QOS: &[i32] = &[1; TOPICS.len()];

#[derive(Error, Debug)]
pub enum MessengerError {
    #[error("Failed to create MQTT client: {0}")]
    FailedToCreateMqttClient(String),

    #[error("Failed to connect to MQTT broker: {0}")]
    FailedToConnectToMqttBroker(String),

    #[error("Failed to disconnect from MQTT broker: {0}")]
    FailedToDisconnectFromMqttBroker(String),

    #[error("Failed to subscribe to messages: {0}")]
    MessageSubscribeFailed(String),

    #[error("Failed to send message: {0}")]
    FailedToSendMessage(String),
}

pub struct Messenger {
    mqtt_client: mqtt::AsyncClient,
    mqtt_stream: mqtt::AsyncReceiver<Option<mqtt::Message>>,
}

impl Messenger {
    pub fn new(config: &NetworkConfig) -> Result<Self, MessengerError> {
        let mqtt_address = format!("tcp://{}:{}", config.mqtt_broker_address, config.mqtt_broker_port);

        let create_opts = mqtt::CreateOptionsBuilder::new()
            .server_uri(&mqtt_address)
            .client_id(&config.mqtt_client_id)
            .finalize();

        let mut mqtt_client =
            mqtt::AsyncClient::new(create_opts).map_err(|e| MessengerError::FailedToCreateMqttClient(e.to_string()))?;
        let mqtt_stream = mqtt_client.get_stream(25);

        Ok(Self {
            mqtt_client,
            mqtt_stream,
        })
    }

    pub async fn connect(&mut self) -> Result<(), MessengerError> {
        let lwt = mqtt::Message::new(api::DISCONNECT, "Connection to broker lost", mqtt::QOS_1);

        let conn_opts = mqtt::ConnectOptionsBuilder::with_mqtt_version(mqtt::MQTT_VERSION_5)
            .clean_start(false)
            .will_message(lwt)
            .finalize();

        self.mqtt_client
            .connect(conn_opts)
            .await
            .map_err(|e| MessengerError::FailedToConnectToMqttBroker(e.to_string()))?;

        let sub_opts = vec![mqtt::SubscribeOptions::with_retain_as_published(); TOPICS.len()];
        self.mqtt_client
            .subscribe_many_with_options(TOPICS, QOS, &sub_opts, None)
            .await
            .map_err(|e| MessengerError::MessageSubscribeFailed(e.to_string()))?;

        self.mqtt_client
            .publish(mqtt::Message::new(api::CONNECT, "", 1))
            .await
            .map_err(|e| MessengerError::FailedToSendMessage(e.to_string()))?;

        Ok(())
    }

    pub async fn run(messenger: Arc<Mutex<Self>>) -> async_std::task::JoinHandle<()> {
        async_std::task::spawn(async move {
            loop {
                if let Some(msg) = messenger.lock().mqtt_stream.next().await {
                    if let Some(msg) = msg {
                        msg.topic();
                    }
                }
            }
        })
    }

    pub async fn send_message<M>(&mut self, topic: &str, message: M) -> Result<(), MessengerError>
    where
        M: Into<Vec<u8>>,
    {
        Ok(self
            .mqtt_client
            .publish(mqtt::Message::new(topic, message, 1))
            .await
            .map_err(|e| MessengerError::FailedToSendMessage(e.to_string()))?)
    }

    pub async fn disconnect(&mut self) -> Result<(), MessengerError> {
        let _res = self
            .mqtt_client
            .disconnect(None)
            .await
            .map_err(|e| MessengerError::FailedToDisconnectFromMqttBroker(e.to_string()))?;

        Ok(())
    }
}
