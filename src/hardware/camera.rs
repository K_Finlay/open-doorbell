//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

use std::{ptr::slice_from_raw_parts, sync::OnceLock, thread::JoinHandle};

pub use camera_clib::CameraConfig;
use chrono::Utc;
use parking_lot::Mutex;
use ringbuffer::{AllocRingBuffer, RingBufferExt, RingBufferWrite};

//--------------------------------------------------------------------------------------------------

pub(crate) mod camera_clib {
    #![allow(non_camel_case_types)]
    #![allow(non_snake_case)]
    #![allow(unused)]
    include!(concat!(env!("OUT_DIR"), "/camera_clib_binding.rs"));
}

//--------------------------------------------------------------------------------------------------
//-- STATICS ---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

static FRAME_BUFFER: OnceLock<Mutex<AllocRingBuffer<ImageBuffer>>> = OnceLock::new();

//--------------------------------------------------------------------------------------------------
//-- STRUCTS ---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[repr(C)]
#[derive(Debug, Clone)]
pub struct ImageBuffer<'a> {
    pub id:        usize,
    pub timestamp: String,
    pub data:      &'a [u8],
}

impl ImageBuffer<'_> {
    pub fn new(handle: camera_clib::ImageBuffer) -> Self {
        Self {
            id:        handle.id as usize,
            timestamp: Utc::now().to_string(),
            data:      unsafe { &*slice_from_raw_parts(handle.imagePtr as *const u8, handle.length as usize) },
        }
    }

    pub const fn id(&self) -> usize {
        self.id
    }

    pub const fn buffer(&self) -> &[u8] {
        self.data
    }

    pub const fn len(&self) -> usize {
        self.data.len()
    }

    pub const fn is_empty(&self) -> bool {
        self.data.is_empty()
    }
}

//--------------------------------------------------------------------------------------------------
//-- PUBLIC FUNCTIONS ------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

pub fn start_streaming(config: CameraConfig) -> JoinHandle<()> {
    init_receiver();
    std::thread::spawn(move || {
        unsafe {
            camera_clib::start_streaming(config, Some(on_frame_received));
        };
    })
}

pub fn get_frame() -> ImageBuffer<'static> {
    FRAME_BUFFER
        .get()
        .expect("Failed to get FRAME_BUFFER!")
        .lock()
        .back()
        .expect("Failed to get image from buffer!")
        .clone()
}

pub fn stop_streaming() {
    unsafe { camera_clib::stop_streaming() };
}

//--------------------------------------------------------------------------------------------------
//-- PRIVATE FUNCTIONS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

extern "C" fn on_frame_received(buffer: camera_clib::ImageBuffer) {
    FRAME_BUFFER
        .get()
        .expect("Failed to get FRAME_BUFFER!")
        .lock()
        .push(ImageBuffer::new(buffer));
}

//--------------------------------------------------------------------------------------------------

fn init_receiver() {
    FRAME_BUFFER
        .set(Mutex::new(AllocRingBuffer::with_capacity(4)))
        .expect("Failed to set FRAME_BUFFER!");
}
