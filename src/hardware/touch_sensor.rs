//--------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

use rppal::gpio::{Gpio, InputPin};

//--------------------------------------------------------------------------------------------------
//-- ENUMS -----------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

#[derive(Copy, Clone, Debug, PartialOrd, PartialEq)]
pub enum TouchState {
    Pressed,
    Released,
}

impl From<bool> for TouchState {
    fn from(value: bool) -> Self {
        match value {
            true => Self::Pressed,
            false => Self::Released,
        }
    }
}

//--------------------------------------------------------------------------------------------------
//-- STRUCTS ---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

pub struct TouchSensor {
    handle:           InputPin,
    last_event:       bool,
    last_event_count: i8,
    callback:         Box<dyn Fn(TouchState)>,
}

impl TouchSensor {
    pub fn new(input_pin: u8, callback: impl Fn(TouchState) + 'static) -> Self {
        Self {
            handle:           Gpio::new().unwrap().get(input_pin).unwrap().into_input(),
            last_event:       false,
            last_event_count: 0,
            callback:         Box::new(callback),
        }
    }

    pub fn update(&mut self) {
        let current_event = self.handle.is_high();

        if current_event == self.last_event {
            self.last_event_count = 0;
            return;
        }

        if self.last_event_count < 5 {
            self.last_event_count += 1;
            return;
        }

        self.last_event_count = 0;
        self.last_event = current_event;

        (self.callback)(TouchState::from(self.last_event))
    }

    pub fn poll_state(&self) -> TouchState {
        TouchState::from(self.last_event)
    }
}
